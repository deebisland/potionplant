import threading
import time


class ProgressBar:
    def __init__(self, canvas, x1, y1, x2, y2, value_max, text, cell, price):
        self.value = 0
        self.value_max = value_max
        self.canvas = canvas
        self.rectangle = self.canvas.create_rectangle(x1, y1, x2, y2, fill="#d88")
        self.coords_rectangle = self.canvas.coords(self.rectangle)
        self.length = (self.coords_rectangle[2] - self.coords_rectangle[0]) / self.value_max
        self.fill_rectangle = self.canvas.create_rectangle(0, 0, 0, 0, fill="red")
        self.text = self.canvas.create_text(x1 + (x2 - x1) / 2, y1 + (y2 - y1) / 2, text=text)
        self.cell = cell
        self.price = int(price)

    def start(self):
        self.value += 1
        # if self.value % 5 != 0:
        #     return
        self.canvas.coords(self.fill_rectangle,
                           self.coords_rectangle[0],
                           self.coords_rectangle[1],
                           self.coords_rectangle[0] + self.value * self.length,
                           self.coords_rectangle[3])

    def __repr__(self):
        return str(self.cell)
