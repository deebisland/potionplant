class Plant:
    def __init__(self, name, cost, price, minimum_factory_level, time_to_make):
        self.name = name
        self.cost = cost
        self.price = price
        self.minimum_factory_level = minimum_factory_level
        self.time_to_make = time_to_make

    def __repr__(self):
        return str(self.name)
