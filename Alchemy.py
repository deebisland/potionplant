class Alchemy:
    def __init__(self, name, description, cost, lifespan, climate, area_requried,
                 level, is_heater_available, is_light_available):
        self.level = level
        self.is_heater_available = is_heater_available
        self.is_light_available = is_light_available
