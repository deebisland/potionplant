import tkinter as tk
import Plant
import ProgressBar
import threading
import time
import Player
import datetime
import Building
from PIL import Image, ImageTk

root = tk.Tk()
root.title("Plant Potion")

canvasWrappersCount = 4
activeCanvas = canvasWrappersCount - 1
canvasWrappers = []
canvasList = []
for index in range(canvasWrappersCount):
    canvasWrappers.append(tk.Frame(root))
    canvasWrappers[index].grid(row=0, column=0)
    canvasList.append(tk.Canvas(canvasWrappers[index], width=700, height=500, bg="#" + str(index) * 3))
    canvasList[index].grid(row=0, column=0)

plants = []
plantFileLineCount = 5
plantFileName = "PLANT.txt"
with open(plantFileName, "r") as plantFile:
    properties = []
    lineCounter = 0
    for line in plantFile:
        line = line.strip("\n")
        properties.append(line)
        lineCounter += 1
        if lineCounter == plantFileLineCount:
            plants.append(Plant.Plant(*properties))
            properties = []
            lineCounter = 0

cells = [[[]]]
for index in range(len(canvasList)):
    for gridY in range(7):
        cells[index].append([])
        for gridX in range(7):
            cells[index][gridY].append(canvasList[index].create_rectangle(gridX * 50 + 50, gridY * 50 + 50,
                                                                          gridX * 50 + 100, gridY * 50 + 100,
                                                                          fill="#ddd"))
    cells[index].pop()
    cells.append([[]])
cells.pop()

plantChosen = tk.StringVar(root)
plantChosen.set(plants[0])
plantOption = tk.OptionMenu(root, plantChosen, *plants)
plantOption.grid(row=2, column=1)
cellsMarked = []
cellsProgressBar = []
x = y = 0
isClickInside = False


def place_mark(event):
    global x, y, isClickInside
    x = int(event.x / 50) - 1
    y = int(event.y / 50) - 1
    if y >= len(cells[activeCanvas]) or x >= len(cells[activeCanvas][0]) or 0 > x or 0 > y:
        return
    for cell in cellsProgressBar:
        if [activeCanvas, y, x] == cell.cell:
            return
    isClickInside = True
    if [activeCanvas, y, x] not in cellsMarked:
        canvasList[activeCanvas].itemconfigure(cells[activeCanvas][y][x], fill="green")
        cellsMarked.append([activeCanvas, y, x])


def seek_mark(event):
    global x, y, isClickInside, cellsMarked
    x_new = int(event.x / 50) - 1
    y_new = int(event.y / 50) - 1
    if y_new >= len(cells[activeCanvas]) or x_new >= len(cells[activeCanvas][0]) or 0 > x_new or 0 > y_new \
            or isClickInside is False:
        return
    if x != x_new and y != y_new:
        for i in range(y, y_new + (1 if y_new > y else -1), -1 if y > y_new else 1):
            for ii in range(x, x_new + (1 if x_new > x else -1), -1 if x > x_new else 1):
                if [activeCanvas, i, ii] not in cellsMarked:
                    cellsMarked.append([activeCanvas, i, ii])
    elif y != y_new:
        for i in range(y, y_new + (1 if y_new > y else -1), -1 if y > y_new else 1):
            if [activeCanvas, i, x] not in cellsMarked:
                cellsMarked.append([activeCanvas, i, x])
    elif x != x_new:
        for ii in range(x, x_new + (1 if x_new > x else -1), -1 if x > x_new else 1):
            if [activeCanvas, y, ii] not in cellsMarked:
                cellsMarked.append([activeCanvas, y, ii])

    def _is_in_cellsProgressBar(cell):
        for cellProgressBar in cellsProgressBar:
            if cell == cellProgressBar.cell:
                return True
        return False

    def _is_in_buildingsPlaced(cell):
        for buildingPlaced in buildingsPlaced:
            if cell in buildingPlaced:
                return True
        return False
    cellsMarked = [cell for cell in cellsMarked if not (_is_in_buildingsPlaced(cell) or _is_in_cellsProgressBar(cell))]
    for cellMarked in cellsMarked:
        canvasList[activeCanvas].itemconfigure(cells[activeCanvas][cellMarked[1]][cellMarked[2]], fill="green")

def reset_mark(event):
    global x, y, isClickInside
    x = y = 0
    isClickInside = False


def remove_mark():
    global cellsMarked
    for cell in cellsMarked:
        canvasList[activeCanvas].itemconfigure(cells[cell[0]][cell[1]][cell[2]], fill="#ddd")
    cellsMarked = []
btnResetMarks = tk.Button(root, text="Reset Marks", command=remove_mark)
btnResetMarks.grid(row=3, column=2)


def place_plant():
    global cellsMarked, player
    cells_marked_count = len(cellsMarked)
    for plant in plants:
        if plant.name == str(plantChosen.get()):
            plant_cost = plant.cost
            plant_value_max = int(plant.time_to_make) * 10  # 10 frames per second todo remove magic number
            plant_price = plant.price
            break
    if player.money - int(plant_cost) * cells_marked_count < 0:
        return
    player.money -= int(plant_cost) * cells_marked_count
    for cell in cellsMarked:
        _cell = cells[cell[0]][cell[1]][cell[2]]
        canvasList[cell[0]].itemconfigure(_cell, fill="#ddd")
        coords = canvasList[cell[0]].coords(_cell)
        cellsProgressBar.append(ProgressBar.ProgressBar(canvasList[cell[0]], coords[0], coords[1],
                                                        coords[2], coords[3], plant_value_max, str(plantChosen.get()),
                                                        cell, plant_price))
    cellsMarked = []
btnPlantPlace = tk.Button(root, text="Place Plant", command=place_plant)
btnPlantPlace.grid(row=2, column=2)

for index in range(len(canvasList)):
    canvasList[index].bind("<Button-1>", place_mark)
    canvasList[index].bind("<B1-Motion>", seek_mark)
    canvasList[index].bind("<ButtonRelease-1>", reset_mark)


def change_canvas(i):
    global activeCanvas
    canvasWrappers[i].lift()
    activeCanvas = i
for index in range(len(canvasList)):
    btnCanvasChange = tk.Button(root, text="floor " + str(index), command=lambda i=index: change_canvas(i))
    btnCanvasChange.grid(row=2 + index, column=3)

playerName = "Deeb"
playerMoney = 50000
playerPlants = []
playerFactoryLevels = [0, 0, 0, 0]
player = Player.Player(playerName, playerMoney, playerPlants, playerFactoryLevels)
playerCanvas = tk.Canvas(root, width=500, height=150, bg="red")
playerCanvas.grid(row=2, column=0, rowspan=4, sticky=tk.W + tk.N)
playerIndex = 0
playerCanvasList = []
for playerProperty in player:
    playerIndex += 1
    playerCanvasList.append(playerCanvas.create_text(20, 20 + playerIndex * 20, text=playerProperty, anchor=tk.W))

buildingCanvas = tk.Canvas(root, width=200, height=150, bg="purple")
buildingCanvas.grid(row=2, column=0, rowspan=4, sticky=tk.E + tk.N)
buildings = []
buildingsPlaced = []
buildingsFileLineCount = 5  # todo not a magic number
buildingFileName = "BUILDING.txt"
with open(buildingFileName, "r") as buildingFile:
    properties = []
    lineCounter = 0
    for line in buildingFile:
        line = line.strip("\n")
        properties.append(line)
        lineCounter += 1
        if lineCounter == buildingsFileLineCount:
            buildings.append(Building.Building(*properties))
            properties = []
            lineCounter = 0
buildingChosen = tk.StringVar(root)
buildingChosen.set(buildings[0])
buildingOption = tk.OptionMenu(root, buildingChosen, *buildings)
buildingOption.grid(row=4, column=1)


def place_building():
    global cellsMarked, player
    cells_marked_count = len(cellsMarked)
    for building in buildings:
        if building.name == str(buildingChosen.get()):
            building_cost = building.cost
            _building = building
            break
    if player.money - int(building_cost) * cells_marked_count < 0:
        return
    player.money -= int(building_cost) * cells_marked_count
    for cell in cellsMarked:
        _cell = cells[cell[0]][cell[1]][cell[2]]
        canvasList[cell[0]].itemconfigure(_cell, fill="#ddd")
        buildingsPlaced.append([cell, _building])
        img = Image.open("AutoFarmer.gif")
        ph_img = ImageTk.PhotoImage(img)
        label = tk.Label(canvasList[cell[0]], image=ph_img, width=50, height=50, borderwidth=0)
        label.image = ph_img
        label.place(y=cell[1] * 50 + 50, x=cell[2] * 50 + 50)
    cellsMarked = []
btnBuildingPlace = tk.Button(root, text="Place Building", command=place_building)
btnBuildingPlace.grid(row=4, column=2)


def game_engine():
    cells_delete = []

    def start_cells(begin, end):
        for cell in cellsProgressBar[begin:end]:
            cell.start()

    def delete_cells(begin, end):
        for cell in cells_delete[begin:end]:
            player.money += cell.price
            activeCanvas = cell.cell[0]
            canvasList[activeCanvas].delete(cell.fill_rectangle)
            canvasList[activeCanvas].delete(cell.rectangle)
            canvasList[activeCanvas].delete(cell.text)
            cellsProgressBar.remove(cell)
            cells_delete.remove(cell)
    global player
    while True:
        time_1 = datetime.datetime.now()
        t = []
        if len(cellsProgressBar):
            start_cells_threads_count = int(len(cellsProgressBar) / 10)
            start_cells_threads_count_modulus = len(cellsProgressBar) % 10
            if start_cells_threads_count == 0:
                t.append(threading.Thread(target=start_cells, args=[0, start_cells_threads_count_modulus]))
            else:
                for i in range(start_cells_threads_count + 1):
                    if i == start_cells_threads_count:
                        t.append(threading.Thread(target=start_cells,
                                                  args=[i * 10, i * 10 + start_cells_threads_count_modulus]))
                    else:
                        t.append(threading.Thread(target=start_cells, args=[i * 10, i * 10 + 10]))
        for th in t:
            th.start()
        for th in t:
            th.join()
        for cell in cellsProgressBar:
            if cell.value == cell.value_max:
                # add output according to direction todo
                cells_delete.append(cell)
        t = []
        if len(cells_delete):
            delete_cells_threads_count = int(len(cells_delete) / 10)
            delete_cells_threads_count_modulus = len(cells_delete) % 10
            if delete_cells_threads_count == 0:
                t.append(threading.Thread(target=delete_cells, args=[0, delete_cells_threads_count_modulus]))
            else:
                for i in range(start_cells_threads_count + 1):
                    if i == start_cells_threads_count:
                        t.append(threading.Thread(target=delete_cells,
                                                  args=[i * 10, i * 10 + delete_cells_threads_count_modulus]))
                    else:
                        t.append(threading.Thread(target=delete_cells, args=[i * 10, i * 10 + 10]))
            for th in t:
                th.start()
            for th in t:
                th.join()
        playerCanvas.delete(playerCanvasList[1])  # removes the money figure, and replaces it with a new money figure
        playerCanvasList.pop(1)
        playerCanvasList.insert(1, playerCanvas.create_text(20, 60, text=player.money, anchor=tk.W))
        time_2 = datetime.datetime.now()
        time_delta = time_2 - time_1
        time_delta_seconds = time_delta.total_seconds()
        if time_delta_seconds < 0.1:
            time.sleep(0.1 - time_delta_seconds)
threading.Thread(target=game_engine, daemon=True).start()

root.mainloop()
