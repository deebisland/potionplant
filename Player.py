class Player:
    def __init__(self, name, money=500, plants=None, factory_levels=None):
        if factory_levels is None:
            factory_levels = []
        self.name = name
        self.money = money
        self.plants = plants
        self.factory_levels = factory_levels

    def __iter__(self):
        self.properties = [self.name, self.money, self.plants, self.factory_levels]
        self.properties_index = -1
        return self

    def __next__(self):  # Python 3: def __next__(self)
        self.properties_index += 1
        if self.properties_index < len(self.properties):
            return self.properties[self.properties_index]
        else:
            raise StopIteration

    def __len__(self):
        return 4  # todo remove magic number
